import { Component, OnInit } from '@angular/core';
import { EventService } from '../shared/event.service';

@Component({
  selector: 'app-event-printer',
  templateUrl: './event-printer.component.html',
  styleUrls: ['./event-printer.component.css']
})
export class EventPrinterComponent implements OnInit {

  public currentMessage: String = "";

  constructor(private eventService: EventService) { }

  ngOnInit(): void {
    // listen for the event and when it happens call the reloadStuff function
    this.eventService.eventEmitter.subscribe(message => this.reloadStuff(message));
  }

  reloadStuff(message: String) {
    this.currentMessage += (message + " ");
  }

}
