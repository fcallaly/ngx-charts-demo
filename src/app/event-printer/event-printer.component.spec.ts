import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventPrinterComponent } from './event-printer.component';

describe('EventPrinterComponent', () => {
  let component: EventPrinterComponent;
  let fixture: ComponentFixture<EventPrinterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EventPrinterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventPrinterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
