import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  // this is used to demonstrate interaction between components
  public eventEmitter: EventEmitter<String> = new EventEmitter<String>();

  constructor() { }
  
  // components call this function to indicate an event has happened
  public emitEvent(message: string) {
    this.eventEmitter.emit(message);
  }
}
