import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { multi } from './data';
import { EventService } from './shared/event.service';
import { RestApiService } from './shared/rest-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'simple-chart';
  chartData: any[] = [{name: "", series: []}];
  view: [number, number] = [700, 300];

  // options
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Date';
  yAxisLabel: string = 'Price $';
  timeline: boolean = true;

  colorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };

  constructor(private restApi: RestApiService,
              private eventService: EventService) {
  }

  ngOnInit(): void {
    this.loadPriceData()
  }

  loadPriceData() {
    this.restApi.getPriceData("TSLA").subscribe((data: any) => {
        console.log(data);
        this.chartData = [{name: data['ticker'],
                           series: data['price_data']}];

        console.log(this.chartData);
    })
  }

  onSelect(data:any): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));

    // any time a chart element is clicked this event is emitted
    this.eventService.emitEvent("item clicked");
  }

  onActivate(data:any): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data:any): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }
}
